﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class audioPlayer : MonoBehaviour
{
    [SerializeField] private List<AudioClip> sounds;
    [SerializeField] private AudioSource audioSource;

    [SerializeField] private GameObject originalStatue;
    [SerializeField] private GameObject brokenStatue;

    private void Update()
    {
        if (!audioSource.isPlaying)
        {
            originalStatue.SetActive(true);
            brokenStatue.SetActive(false);

            if (sounds.Count == 0)
            {
                gameObject.SetActive(false);
            }
        }        
    }

    private void OnMouseDown()
    {
        if(sounds.Count > 0)
        {
            int position = Random.Range(0, sounds.Count);
            audioSource.clip = sounds[position];
            audioSource.Play();
            sounds.RemoveAt(position);

            originalStatue.SetActive(false);
            brokenStatue.SetActive(true);
        }        
    }
}
